pragma solidity ^0.4.24;

import "./openzeppelin-solidity/Whitelist.sol";
import "./openzeppelin-solidity/Ownable.sol";

contract BookingContract is Whitelist{

	uint8 constant TIMESLOT_MIN_VALUE = 0;
	uint8 constant TIMESLOT_MAX_VALUE = 23;

	struct Booking {
		bytes32 userId;
		bool isActive;
	}
	
	//roomId => (timeSlot => Booking)
	mapping(bytes32 => mapping(uint8 => Booking)) bookings;
	bytes32[] public rooms;
	
	constructor() public {
		addAddressToWhitelist(msg.sender);
		
		//The rooms are part of the deal between the two companies, so they are initialized by the contract.
		rooms.push("C01");
		rooms.push("C02");
		rooms.push("C03");
		rooms.push("C04");
		rooms.push("C05");
		rooms.push("C06");
		rooms.push("C07");
		rooms.push("C08");
		rooms.push("C09");
		rooms.push("C10");
		
		rooms.push("P01");
		rooms.push("P02");
		rooms.push("P03");
		rooms.push("P04");
		rooms.push("P05");
		rooms.push("P06");
		rooms.push("P07");
		rooms.push("P08");
		rooms.push("P09");
		rooms.push("P10");
	}
	
	function clearAllBookings() public onlyOwner (){

		for (uint8 j=0; j<rooms.length; j++){
			for (uint8 i=TIMESLOT_MIN_VALUE; i<TIMESLOT_MAX_VALUE; i++) {
				if(bookings[rooms[j]][i].isActive){
					bookings[rooms[j]][i].userId = keccak256("");
					bookings[rooms[j]][i].isActive = false;
				}
			}
		}
	}
	
	function addBooking(bytes32 roomId, uint8 timeSlot, bytes32 userId) public onlyWhitelisted(){
		require(timeSlot >= TIMESLOT_MIN_VALUE && timeSlot <= TIMESLOT_MAX_VALUE);
		require(!bookings[roomId][timeSlot].isActive);
		require(contains(rooms, roomId));
		
		Booking memory booking;
		booking.userId = userId;
		booking.isActive = true;
		
		bookings[roomId][timeSlot] = booking;
	}
	
	function cancelBooking(bytes32 roomId, uint8 timeSlot, bytes32 userId) public onlyWhitelisted(){
		require(timeSlot >= TIMESLOT_MIN_VALUE && timeSlot <= TIMESLOT_MAX_VALUE);
		require(bookings[roomId][timeSlot].isActive);
		require(bookings[roomId][timeSlot].userId == userId);
		
		bookings[roomId][timeSlot].userId = keccak256("");
		bookings[roomId][timeSlot].isActive = false;
	}
	
	function getBookings(bytes32 roomId) public view returns(uint8[]){
		
		uint8[] memory result = new uint8[](TIMESLOT_MAX_VALUE);
		uint8 arrayIndex = 0;
		
		for (uint8 i=TIMESLOT_MIN_VALUE; i<TIMESLOT_MAX_VALUE; i++) {
			if(bookings[roomId][i].isActive){
				result[arrayIndex] = i;
				arrayIndex++;
			}
		}
		return(truncateArray(result, arrayIndex));
	}
	
	function getUserBookings(bytes32 roomId, bytes32 userId) public view returns(uint8[]){
			
		uint8[] memory result = new uint8[](TIMESLOT_MAX_VALUE);
		uint8 arrayIndex = 0;
		
		for (uint8 i=TIMESLOT_MIN_VALUE; i<TIMESLOT_MAX_VALUE; i++) {
			if(bookings[roomId][i].isActive && bookings[roomId][i].userId == userId){
				result[arrayIndex] = i;
				arrayIndex++;
			}
		}
		
		return(truncateArray(result, arrayIndex));
	}
	
	function getAllRooms() public view returns(bytes32[]){
		return(rooms);
	}
	
	
	function truncateArray(uint8[] array, uint8 newLength) internal pure returns(uint8[]) {
	
		uint8[] memory result = new uint8[](newLength);
		for (uint8 i=0; i<newLength; i++) {
			result[i] = array[i];
		}
		return(result);
    }
	
	function contains(bytes32[] array, bytes32 item) internal pure returns(bool) {
		for (uint8 i=0; i<array.length; i++) {
			if(array[i] == item){
				return true;
			}
		}
		return false;
    }
}