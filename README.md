
# Booking Contract

Generated using [Truffle Box](https://truffle-box.github.io/) 

## Getting Started

This repository contains the smart contract for the Room Booking solution

### Prerequisites

* Node JS installed
* A running etherum network (Ganache for development purpose)

### Installing

Download dependencies

```
npm install
```

### Interacting with the smart contract

Use the following commands to compile, test and deploy a smart contract.

These commands works only if you are a working rpc listenning on http://127.0.0.1:8545/, if not, see the next section.

```
truffle compile
truffle test
truffle migrate
```

## Running on a test rpc

This command will run a test rpc on http://127.0.0.1:9545/

```
truffle develop --log &
```

Then run the tests on this network using the networks defined in truffle-config.json

```
truffle test --network test
```

## Export to Web3j

After a sucessfull build of the smart contract you can generate a Java class in order to interact with your smart contract using Web3J.

```
web3j truffle generate build/contracts/BookingContract.json -o ./ -p com.booking.contract
```